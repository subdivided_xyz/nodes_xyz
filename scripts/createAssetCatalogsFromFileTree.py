#!/usr/bin/python3

import os
import sys
import uuid
import argparse
import shutil


parser = argparse.ArgumentParser(description='Generate an updated blender_assets.cats.txt')
parser.add_argument('--write', help="If given, update blender's catalog file of the "+
                                    "current dir. If not, print content to stdout.",
                    action='store_true')
args = parser.parse_args()


pathsUUIDs = {}
ignored_dirs = [".git", ".vscode"]

catalog_file = "./blender_assets.cats.txt"

if os.path.exists(catalog_file):
    with open(catalog_file) as f:
        for line in f.readlines():
            if not line.startswith("#") and ":" in line:
                line_uuid = line.split(":")[0].strip()
                path = line.split(":")[1].strip()
                pathsUUIDs[path] = line_uuid

f = None
if args.write:
    if os.path.exists(catalog_file):
        shutil.copyfile(catalog_file, catalog_file+".bak")
    f = open(catalog_file, "w")
else:
    f = sys.stdout

print("# Autogenerated folder based asset catalogs", file=f)
print("", file=f)
print("VERSION 1", file=f)
catalog_paths = []
for root,dirs,files in os.walk("."):
    traverse_dirs = []
    for dir in dirs:
        if dir in ignored_dirs:
            continue
        path = os.path.join(root, dir)
        if not os.path.exists(os.path.join(path, ".catalogignore")):
            catalog_path = path.replace("./", "")
            catalog_paths.append(catalog_path)
            if not os.path.exists(os.path.join(path, ".nosubcatalogs")):
                traverse_dirs.append(dir)
    dirs[:] = traverse_dirs
catalog_paths.sort()
for catalog_path in catalog_paths:
    line_uuid = str(uuid.uuid4())
    if catalog_path in pathsUUIDs:
        line_uuid = pathsUUIDs[catalog_path]
    print(":".join([line_uuid, catalog_path])+":", file=f)
