#!/usr/bin/bash

if [[ $# -ne 1 ]]; then
    echo "Usage: $0 blend-file"
    exit 1
fi

/opt/blender-3.3/blender -b "$1"  -P ~/blender/mark_scene_entities_as_asset.py