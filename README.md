# nodes_xyz

A collection of my commonly used blender node groups (shading nodes, geometry nodes, ...)

All the content is prepared to be used with blender's asset browser. Just add the root directory of this repository to
the asset libraries in the preferences and enjoy all the content.

## Getting started

To get the nodes either download the respecive blend file directly or download the full repository as a zip file using
this button (![download-icon](icons/download.png)) above.

To easily get access to the provided nodes, activate the
[Node Presets](https://docs.blender.org/manual/en/dev/addons/node/node_presets.html) add-on and configure it to parse
one of the subdirectories (e.g. [shadingNodes](./shadingNodes)).

As soon as the asset browser supports node groups in an official release, this organization might change a bit to
support the asset workflow.

# List of nodes

## Shading utility nodes

### [HSV_Object_Variation](shadingNodes/shadingNodeGroupCollection.blend)

> This node can be integated whenever a whole set of instances is being shaded (e.g. a bunch of rocks). It allows to
> vary each instance in hue, saturation and value.

### [If](shadingNodes/shadingNodeGroupCollection.blend)

> Switches between 2 vector values based on a boolean input.

### [Object Size](shadingNodes/shadingNodeGroupCollection.blend)

> A node, that can be used when shading multiple instances, that are scaled in size. Through this node, it's possible
> to get a factor of the size differences between the instances and align texture coordinates based on this factor.

### [Spherical Coords](shadingNodes/shadingNodeGroupCollection.blend)

> A node, that takes e.g. a shading position and calculates the angles of spherical coordinates.

### [Voronoi Based Noise](shadingNodes/shadingNodeGroupCollection.blend)

> A kind of noise node, that is based on a voronoi pattern

### [InteriorMapping](shadingNodes/interiorMapping.blend)

<img src="shadingNodes/rooms/room_1x1_1002.png" height=200/>

> This is a shading node, that computes texture coordinates to make an image of a rendered room, like above, having
> depth. It can handle generating UVs for multiple rooms organized with UDIMs. The folder ` shadingNodes/rooms/`
> contains 4 example rooms with 2 different sizes. The example scene demonstrates how to use them and also how to set up
> the UVs of the quads representing the windows into the rooms.

## Full materials

### [Water](shadingNodes/water.blend)

<img src="shadingNodes/water.png" height=200/>

> A volumetric water material.

## World shader nodes

### [LocalEnvUV](worldNodes/halfDomeEnv.blend)

<img src="worldNodes/halfDomeEnv.png" height=200 />

> A node, that calculates environment map UVs for realizing a flat ground on an HDRI based world environment. Just
> connect this node to your environment image and adjust the size, camera height and the orientation angle to setup the
> HDRI.


## Geometry nodes

### [BBoxField](geoNodes/deformObjectsToCurve.blend) (Blender 3.1)

> Contained within the `deformObjectsToCurve.blend` file, this node provides a field output, that defines the bounding
> box of each instance being provided through the Instances parameter.

### [BoxDeform](geoNodes/deformObjectsToCurve.blend) (Blender 3.1)

> Also contained within the `deformObjectsToCurve.blend`  file, this node takes a normalized geometry (e.g. contained
> within the [0..1] cube) and interpolates the point positions to a box provided by the 8 corner input parameters.

### [Bricker](geoNodes/bricker.blend) (Blender 3.4)

<img src="geoNodes/bricker.webp" height=200/>

> This group takes a set of polygons lying flat on the xy plane. It then subdivides all of them along the principal x-
> and y-axes. Options are whether the quads of the grid are kept only if lying completely within the original polygon or
> if a partial overlap is okay. For partial overlap it's also possible, to align the outside points to the original
> geometry. In that case, the boundary layer wil not be squares anymore, but the geometry is aligning much closer to the
> original one.

### [Center Instance Locations](geoNodes/fracture.blend) (Blender 4.0)

<img src="geoNodes/centerInstanceLocations.webp" height=200/>

> This node group takes a geometry with a couple of instances and centers their location/origin.

### [Clustered Random](geoNodes/clusteredRandom.blend) (Blender 3.5)

<img src="geoNodes/circleClustersSampleRandom.webp" height=200/>

> This is a set of 2 geometry nodes. One to take a collection and arrange all instances in a set of clustered circles.
> Clusters are built based on the position of the instances. The second node takes these rearranged instances to sample
> a random element from one of the clusters. This can e.g. be used for building creation: Clusters would be the story of
> the building, the length of the element (1 unit, 2 units, ...).

> Attributes:
 * **Separate on x-Axis** : If the collection forms something like walls, we can layout the wall panels along the x-axis
   and not use it for clustering criteria. This allows to model good continuity between the panels.
 * **Cluster Geometry**: The output of the CircleClustersCreate node into the sampling node.
 * **Criteria X,Y,Z**: These 3 inputs of the random sample node are fields, that will be evaluated to determine the
   right cluster to sample from.
 * **Seed**: Randomize the sampling

### [CurvePointInfo](geoNodes/cutSharpCorners.blend) (Blender 3.4)

<img src="geoNodes/curvePointInfo.webp" height=200/>

> This is a small utility node within the cutSharpCorners node. It provides extended information about a point in a
> curve. Information is the previous and next point positions, the directions to those points and the angle between
> these 2 edges.

### [CutSharpCorners](geoNodes/cutSharpCorners.blend) (Blender 3.4)

<img src="geoNodes/cutSharpCorners.webp" height=200>

> This geometry node analyzes the given input curves and cuts away those corners, that are sharper than a given
> threshold.

### [Deform Objects to curve](geoNodes/deformObjectsToCurve.blend) (Blender 3.1)

<img src="geoNodes/deformObjectsToCurve.jpg" height=200/>

> This node group aligns a set of objects on a curve and additionally allows deforming them along the curve itself.

### [Fracture](geoNodes/fracture.blend) (Blender 4.0)

<img src="geoNodes/fracture.webp" height=200 />

> This node group divides an object into a certain amount of individual pieces. The idea is to be able to
> feed these instances at some point in time into a rigid body simulation. Until simulation nodes are up to
> that capability, thee's still the possibility to bake the geometry node and separate the individual pieces
> into their own objects. This can then be simulated with the existing RBD engine.

### [IslandCenterField](geoNodes/islandCenterField.blend) (Blender 3.1)

<img src="geoNodes/islandCenterField.jpg" height=200 /> 

> A field node, that returns the center point of each mesh island.

### [Lerp](geoNodes/deformObjectsToCurve.blend) (Blender 3.1)

> **Deprecated**: Starting from Blender 3.4 one can use the float version of the mix node.
>
> Contained within the `deformObjectsToCurve.blend` file, this is a simple node, that interpolates between 2 vector
> values based on a numeric input t, which is given in the range of [0..1].

### [Normalized Blur](geoNodes/normalizedBlur.blend) (Blender 4.0)

<img src="geoNodes/normalizedBlur.webp" height=200/>

> A node, that computes a soft selection. Feeding in a selection through a boolean field, it blurs the field,
> but it also ensures, that the originally selected points will still be selected 100%.

### [OffsetCurve](geoNodes/offsetCurve.blend) (Blender 3.4)

<img src="geoNodes/offsetCurve.webp" height=200/>

> This group generates a curve, that is a specified distance apart from the input curve.
> It works also on non-planar curves and concave ones. 

### [Particles and GeoNodes](geoNodes/particlesAndGeoNodes.blend) (Blender 4.2)

<img src="geoNodes/particlesAndGeoNodes.webp" height=200/>

> This group works together with a python script also embedded in the demo file. It bakes particle movement to a mesh
> and uses a geometry nodes graph to reconstruct the movement. This allows e.g. to use child particles together with
> attributes like age or particle ID in the shader graph.

### [PatternResample](geoNodes/patternResample.blend) (Blender 3.5)

<img src="geoNodes/patternResample.webp" height=200/>

> This group can resample a curve into segments, following a length pattern. For example a segment of length 2
> followed by two segments of length 1. If the curve is longer than the sum of the provided segments, the pattern
> is repeated.

### [Plastic Deform](geoNodes/plasticDeform.blend) (Blender 4.0)

<img src="geoNodes/plasticDeform.webp" height=200 />

> This group is a simulation group. It deforms an object based on the intersections of another object.

### [ResetInstances](geoNodes/resetInstances.blend)(Blender 3.5)

<img src="geoNodes/resetInstances.webp" height=200/>

> This group takes a set of instances and resets their position and orientation back to the origin.
> This is similarly to checking the flag "*Reset Children*" on the "*Collection Info*" node.

### [SelectVectorAxis](geoNodes/patternResample.blend) (Blender 3.5)

<img src="geoNodes/selectVectorAxis.webp" height=200 />

> A node, that extracts a specific component of a vector. Similar to the built-in SeparateXYZ node, but
> determination of which component to use is done dynamically through an axis attribute on the node.

### [SetUpAxisTilt](geoNodes/setUpAxisTilt.blend) (Blender 3.1)

<img src="geoNodes/setUpAxisTilt.jpg" height=200 />

> A node, that mimics blender's native z-Up curve orientation by calculating the corresponding tilt angles.

### [ShapeGrammar](geoNodes/shapeGrammar.blend) (Blender 4.0)

<img src="geoNodes/shapeGrammar.png" height=200 />

> A geometry node, that takes a grammar rule as input (e.g. "AB?(CB)*A") together with a collection and a curve
> instances respective elements from the collection onto the curve. Rules are the following:
> * A-Z : references a geometry from the collection (A=first geometry, B=second, C=third, ...)
> * (...): groups elements together
> * ?: Marks a geometry or group as optional
> * *: Marks a geometry or group as repeatable
>
> For example the grammar from above ("AB?(CB)*A") can be expanded to a length of 9 in the following way:
> ABCBCBCBA
>
> So the resulting geometry is framed by instances of A, while inside instances of B and C alternate.

### [StringToField](geoNodes/patternResample.blend) (Blender 3.5)

<img src="geoNodes/stringToField.webp" height=200/>

> This node takes a string of up to 10 characters and generates an integer field from it.

### [Tissue](geoNodes/tissue.blend) (Blender 3.1)

<img src="geoNodes/tissue.jpg" height=200/>

> This is a geo node group, that mimicks what the
> [Tissue](https://docs.blender.org/manual/en/latest/addons/mesh/tissue.html) addon provided with blender is doing.

### [VisualizeInt](geoNodes/visualizeInt.blend) (Blender 3.1)

<img src="geoNodes/visualizeInt.jpg" height=200 />

> Three nodes to debug node trees. They take a geometry and an integer field to display the field numbers at the
> respective geometrical elements. Available implementations:
> * VisualizePointInt
> * VisualizeEdgeInt
> * VisualizeFaceInt
> 
> Screenshot above shows the edge visualizer.

### [VisualizeVector](geoNodes/visualizeVector.blend) (Blender 3.1)

<img src="geoNodes/visualizeVector.jpg" height=200 />

> Four nodes to debug node trees. They take a geometry and a vector field. It evaluates the field at each respective
> geometrical element and places an arrow which represents the vector of the field visually with its direction and
> magnitude. Available implementations:
> * VisualizePointVector
> * VisualizeEdgeVector
> * VisualizeFaceVector
> * VisualizeFaceCornerVector

# Assets

The repository also will contain a list of small assets, that can be reused in multiple projects.

## Coordinate System

[<img src="assets/coordinateSystem.png" height=200/>](assets/coordinateSystem.blend)

## Eye

[<img src="assets/eye.png" height=200/>](assets/eye.blend)

## UFO

[<img src="assets/ufo.png" height=200/>](assets/ufo.blend)

> A procedural UFO, that utilizes geometry nodes to build the spaceship from a cross section curve. This model
> additionally is rigged and has a laser mounted at the bottom.

# Scripts

The [scripts](scripts/) directory contains some little utility scripts, that are useful for asset handling.

* [createAssetCatalogsFromFileTree.py](scripts/createAssetCatalogsFromFileTree.py) takes a directory structure and
  creates/updates a catalog file for blender's asset browser.
* [mark_scene_entities_as_asset.sh](scripts/mark_scene_entities_as_asset.sh) is a little script, that takes a scene file
  and marks the content as assets. It looks into the scene file and if there's only a single object, this objects gets
  marked as an asset, otherwise all top level collections will be assets. Additionally is finds the right catalog to
  sort the assets into, ensures there's a preview image and if needed even renders one.